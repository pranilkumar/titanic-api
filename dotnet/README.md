# TitanicAPI for C#

TitanicAPI is a simple REST application which offers CRUD operations for existing dataset.
The application assumes there's a SqlServer running locally, the connection string can be changed via the [appsettings.json](./TitanicAPI.appsettings.json) file.
If you copy the [titanic.csv](../titanic.csv) file into the [TitanicAPI](./TitanicAPI) folder the app will try to populate the database with th csv file.

## Supported frameworks

- .NET 5.0
- Visual Studio 2019 (or Visual Studio Code)

