﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TitanicAPI.CsvImport;

namespace TitanicAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using ILoggerFactory loggerFactory =
                LoggerFactory.Create(builder =>
                    builder.AddSimpleConsole(options =>
                    {
                        options.IncludeScopes = true;
                        options.SingleLine = true;
                        options.TimestampFormat = "hh:mm:ss ";
                    }));

            ILogger<Program> logger = loggerFactory.CreateLogger<Program>();
            try
            {
                var importer = new CsvImporterToDb();
                importer.ImportCsv();
                CreateHostBuilder(args).Build().Run();
            }
            catch (SqlException sqlex)
            {
                logger.LogError(sqlex, "Error trying to connect to Database");
            }
            catch (FileNotFoundException fnfex)
            {
                logger.LogError(fnfex, "Error trying to find file:", fnfex.FileName );
            }
            catch (Exception ex)
            {
                logger.LogCritical(ex, "Unexpected error occurred");
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}