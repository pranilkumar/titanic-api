﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.Data.SqlClient;
using TitanicAPI.Models;

namespace TitanicAPI.CsvImport
{
    public class CsvImporterToDb
    {
        public void ImportCsv()
        {
            using var db = new PeopleDb();
            db.Database.EnsureCreated();
            if (!db.People.Any())
            {
                var config = new CsvConfiguration(CultureInfo.InvariantCulture)
                {
                    IgnoreBlankLines = false,
                    //The .csv doesn't contain uuid header/information. Disable the checks and let the Entity Framework generate the uuid
                    MissingFieldFound = null,
                    HeaderValidated = null,
                };

                using var reader = new StreamReader("titanic.csv");
                using var csv = new CsvReader(reader, config);
                var people = csv.GetRecords<Person>();

                db.AddRange(people);
                db.SaveChanges();
            }
        }
    }
}
